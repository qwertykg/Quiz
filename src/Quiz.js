import * as PIXI from 'pixi.js'

class Quiz {

    constructor() {    

        const app = new PIXI.Application();
        app.width = window.innerWidth;
        document.body.appendChild(app.view);

        PIXI.Loader.shared.add('pixijs', 'pixi.js').load((loader, resources) => 
        {

            var style = new PIXI.TextStyle({
                fontFamily: 'Arial',
                fontSize: 20,
                fontStyle: 'italic',
                fontWeight: 'bold',
                fill: ['#ffffff', '#00ff99'], // gradient
                stroke: '#4a1850',
                strokeThickness: 5,
                dropShadow: true,
                dropShadowColor: '#000000',
                dropShadowBlur: 4,
                dropShadowAngle: Math.PI / 6,
                dropShadowDistance: 6,
                wordWrap: true,
                wordWrapWidth: 800
                });
        
        var richText = new PIXI.Text('Key = TRUMPED', style);
        
        richText.x = 30;
        richText.y = 100;
    
        app.stage.addChild(richText);
    
        var richText = new PIXI.Text('Format: [T][H][I][S]*[I][S]*[T][H][E]*[F][O][R][M][A][T]', style);
        
        richText.x = 30;
        richText.y = 150;
    
        app.stage.addChild(richText);
    
        var richText = new PIXI.Text('Hidden message: [21][19][3][2][10][6]*[1][2][6][6]*[14][21]*[8]*[9][6][1][1][6][2]* [8][17][1][6][2][18][8][1][14][22][6][1][19]*[12][14][1]*[16][2][8][16][6][18]', style);
        
        richText.x = 30;
        richText.y = 200;
    
        app.stage.addChild(richText);         
        });

    }
}

export default Quiz;