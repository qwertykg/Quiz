const gulp = require("gulp");
var babelify = require('babelify');
const connect = require('gulp-connect');
var browserify = require('browserify');
var source = require('vinyl-source-stream');

var babelifyConfig = {
    presets: [
        'es2015',
        'stage-0'
    ],
    sourceMaps: 'both',
    plugins: [
        'transform-object-assign',
        'es6-promise'
    ]
};

gulp.task('browserify', function() {
    return browserify('./src/index.js', { debug: true, standalone: 'index' })
        .transform(babelify, babelifyConfig)
        .bundle()
        .pipe(source("index.js"))
        .pipe(gulp.dest("./bin"));
});

gulp.task('html', () => {
    return gulp.src(['./resources/index.html', './resources/pixi.js', './resources/test.html'])
        .pipe(gulp.dest('./bin'));
});

gulp.task('serve', function() {
    connect.server({
      root: 'bin/',
      livereload: true,
      port: 3000
    });
  });

gulp.task('default', gulp.series('browserify', 'html', 'serve'));